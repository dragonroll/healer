extends Node2D

func _ready():
	for node in $Characters.get_children():
		connect_spawn_signal(node)

func connect_spawn_signal(node: Node):
	for signal_data in node.get_signal_list():
		if signal_data["name"] == "spawn_requested":
			#warning-ignore:return_value_discarded
			node.connect("spawn_requested", self, "spawn")

func spawn(node: Node):
	connect_spawn_signal(node)
	$Characters.add_child(node)
