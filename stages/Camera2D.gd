extends Camera2D


export var healer_path: NodePath

func _process(delta):
	var healer = get_node(self.healer_path)
	if healer.position.x > self.position.x + 160:
		var diff = healer.position.x - self.position.x + 160
		self.position.x += diff * delta * 100
