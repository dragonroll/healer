extends Reference

class_name BGMPlayer

var bgm

func _init(tree: SceneTree):
	self.bgm = tree.get_root().get_node("BGM")

func play(name: String):
	if self.bgm.current != name:
		if self.bgm.current != null:
			self.bgm.get_node(self.bgm.current).stop()
		fade_in(name)
		self.bgm.current = name

func stop():
	if self.bgm.current != null:
		fade_out(self.bgm.current)
		self.bgm.current = null

func fade_in(name):
	bgm.get_node(name).volume_db = 0
	bgm.get_node(name).play()

func fade_out(name):
	var tween := bgm.get_node("FadeOut") as Tween
	#warning-ignore:return_value_discarded
	tween.interpolate_property(bgm.get_node(name), "volume_db", 0, -80, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	#warning-ignore:return_value_discarded
	tween.start()
	yield(tween, "tween_completed")
	bgm.get_node(name).stop()
