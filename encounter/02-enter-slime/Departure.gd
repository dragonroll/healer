extends Node

class_name Departure

onready var bgm_player := BGMPlayer.new(get_tree())

export var encounter_path: NodePath
export var next_encounter: PackedScene

func _play():
	self.bgm_player.stop()
	yield(get_tree().create_timer(2), "timeout")
	self.bgm_player.play("Stride")
	var encounter = get_node(self.encounter_path)
	encounter.disable_player_control()
	encounter.disable_limits()
	yield(get_tree().create_timer(2), "timeout")
	for hero in get_tree().get_nodes_in_group("party"):
		hero.change_speed(16)
		hero.get_node("Event").move_during(1, 60)
	encounter.camera_out()
	yield(encounter.get_node("Tween"), "tween_completed")
	for hero in get_tree().get_nodes_in_group("party"):
		hero.visible = false
	if self.next_encounter != null:
		#warning-ignore:return_value_discarded
		get_tree().change_scene_to(self.next_encounter)
