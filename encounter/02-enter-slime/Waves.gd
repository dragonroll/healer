extends Node

class_name Waves

export (Array, PackedScene) var monsters = []
export (Array, NodePath) var spawn_paths = []
export (Array, float) var delays = []
export var bgm: String = "Combat"

onready var bgm_player := BGMPlayer.new(get_tree())

signal spawn(monster)
signal waves_ended

func _play():
	self.bgm_player.stop()
	for i in range(monsters.size()):
		var monster_scn = monsters[i]
		if monster_scn != null:
			var spawn_node = get_node(self.spawn_paths[i])
			var monster = monster_scn.instance() as Node2D
			monster.position = spawn_node.position
			emit_signal("spawn", monster)
			self.bgm_player.play(self.bgm)
		var delay = delays[i]
		if delay != null:
			yield(get_tree().create_timer(delay), "timeout")
	while true:
		if get_tree().get_nodes_in_group("monster").size() <= 0:
			emit_signal("waves_ended")
			return
		yield(get_tree(), "physics_frame")
