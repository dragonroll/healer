extends Node

class_name Entrance

export(Array, int) var skills_available = [0]

onready var bgm_player := BGMPlayer.new(get_tree())

signal entrance_ended

func _play(encounter: Encounter):
	bgm_player.play("Stride")
	encounter.disable_player_control()
	encounter.disable_limits()
	for hero in get_tree().get_nodes_in_group("party"):
		hero.visible = false
	encounter.camera_in()
	yield(encounter.get_node("Tween"), "tween_completed")
	for hero in get_tree().get_nodes_in_group("party"):
		hero.visible = true
		hero.change_speed(32)
		hero.get_node("Event").move_during(1, 4)
	yield(get_tree().create_timer(3.0), "timeout")
	encounter.enable_player_control(self.skills_available)
	encounter.enable_limits()
	for hero in get_tree().get_nodes_in_group("party"):
		hero.restore_speed()
	emit_signal("entrance_ended")
