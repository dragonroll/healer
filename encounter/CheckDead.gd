extends Node

func _play(_encounter):
	var party = get_node("/root/Party")
	for hero in get_tree().get_nodes_in_group("party"):
		if hero.name in party.dead:
			hero.queue_free()
