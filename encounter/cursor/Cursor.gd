extends Area2D

export (NodePath) var camera_path

signal entity_clicked(entity)
signal ground_clicked(position)

onready var focus_ref = null

func _ready():
	set_process(false)

func _process(_delta: float):
	_snap_position()
	_update_closest()

func _update_closest():
	var old = self.focus_ref
	if old != null:
		old = old.get_ref()
	_clear_focus()
	var closest = _closest()
	if closest != null:
		closest.focus()
		if closest != old and old != null:
			$ChangeSFX.play()
		self.focus_ref = weakref(closest)
		$Label.text = closest.name

func _unhandled_input(event):
	if self.is_processing():
		if self._is_click(event):
			if self.focus_ref != null and self.focus_ref.get_ref() != null:
				emit_signal("entity_clicked", self.focus_ref.get_ref())
			else:
				emit_signal("ground_clicked", self.position)

func _snap_position():
	var mouse_pos = self.get_viewport().get_mouse_position()
	self.position = _snap(mouse_pos)

func _snap(pos: Vector2) -> Vector2:
	assert(camera_path != null)
	var camera = self.get_node(self.camera_path)
	return pos * camera.zoom

func _is_click(event) -> bool:
	if event.is_action_pressed("click"):
		return true
	elif event is InputEventScreenTouch and event.pressed:
		self.position = _snap(event.position)
		_update_closest()
		return true
	return false

func _closest() -> Node2D:
	var characters = get_tree().get_nodes_in_group("party")
	var mindist := INF
	var closest = null
	for character in characters:
		var pos = character.position
		var dist : float = pos.distance_squared_to(self.position)
		if dist < mindist:
			mindist = dist
			closest = character
	return closest

func _clear_focus():
	if self.focus_ref != null:
		if self.focus_ref.get_ref() != null:
			self.focus_ref.get_ref().unfocus()
		self.focus_ref = null
