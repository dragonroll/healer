extends ColorRect

onready var bgm_player := BGMPlayer.new(get_tree())

func _process(_delta):
	var heroes = get_tree().get_nodes_in_group("party")
	var gameover = true
	for hero in heroes:
		if hero.name == "Healer":
			gameover = false
	if gameover:
		bgm_player.stop()
		var party = get_node("/root/Party")
		party.dead = []
		var full_color := self.color as Color
		full_color.a = 1
		set_process(false)
		yield(get_tree().create_timer(0.5), "timeout")
		get_tree().paused = true
		$Tween.interpolate_property(self, "color", self.color, full_color, 3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween.start()
		yield($Tween, "tween_completed")
		yield(get_tree().create_timer(2.0), "timeout")
		get_tree().paused = false
		#warning-ignore:return_value_discarded
		var save = party.checkpoint
		if save == null:
			save = "res://encounter/01-dawn-of-victory/Encounter.tscn"
		get_tree().change_scene(save)
