extends Node

export var encounter_path: NodePath
export var next_encounter: PackedScene

func _physics_process(_delta):
	for hero in get_tree().get_nodes_in_group("party"):
		if hero.name == "Swordsman" and hero.get_node("Life").is_full():
			set_physics_process(false)
			play()

func play():
	var encounter = get_node(self.encounter_path)
	encounter.disable_player_control()
	encounter.disable_limits()
	for hero in get_tree().get_nodes_in_group("party"):
		hero.change_speed(16)
		match hero.name:
			"Healer":
				hero.get_node("Event").move_during(1, 60)
			"Swordsman":
				hero.get_node("Event").move_during(1, 60)
	encounter.camera_out()
	yield(encounter.get_node("Tween"), "tween_completed")
#	var passed := false
#	while not passed:
#		passed = true
#		for hero in get_tree().get_nodes_in_group("party"):
#			if hero.position.x < 320 + 32:
#				passed = false
#		yield(get_tree(), "physics_frame")
	#warning-ignore:return_value_discarded
	get_tree().change_scene_to(self.next_encounter)
