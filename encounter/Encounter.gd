extends Control

class_name Encounter

onready var current_skill = null
onready var limits_layer := 0
onready var limits_mask := 0

signal encounter_begun(encounter)

func _ready():
	for skill in $HUD/SkillBar/Icons.get_children():
		skill.connect("pressed", self, "_on_skill_pressed", [skill])
	emit_signal("encounter_begun", self)

func _unhandled_input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()
	if event.is_action_pressed("fullscreen"):
		OS.window_fullscreen = not OS.window_fullscreen

func _on_skill_pressed(skill: Skill):
	self.current_skill = skill
	$Cursor.set_process(not skill.free_target)

func _on_character_clicked(character: Character):
	assert(self.current_skill != null)
	self.current_skill.apply_on_character(character)

func disable_player_control():
	$Stage/Characters/Healer/PlayerControl.enabled = false
	for skill in $HUD/SkillBar/Icons.get_children():
		(skill as TextureButton).disabled = true

func enable_player_control(skills):
	$Stage/Characters/Healer/PlayerControl.enabled = true
	for index in skills:
		var skill = $HUD/SkillBar/Icons.get_child(index)
		(skill as TextureButton).disabled = false

func disable_limits():
	self.limits_layer = $Stage/Camera2D/Limits.collision_layer
	self.limits_mask = $Stage/Camera2D/Limits.collision_mask
	$Stage/Camera2D/Limits.collision_layer = 0
	$Stage/Camera2D/Limits.collision_mask = 0

func enable_limits():
	$Stage/Camera2D/Limits.collision_layer = self.limits_layer
	$Stage/Camera2D/Limits.collision_mask = self.limits_mask

func camera_in():
	var tween = $Tween
	tween.interpolate_property($Stage/Camera2D, "position", Vector2(-320, 0), Vector2(0, 0), 2, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()

func camera_out():
	var tween = $Tween
	tween.interpolate_property($Stage/Camera2D, "position", Vector2(0, 0), Vector2(320, 0), 2, Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.start()
