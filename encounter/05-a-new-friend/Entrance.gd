extends Node

onready var bgm_player := BGMPlayer.new(get_tree())

export (String, FILE, "*.tscn") var checkpoint

func _play(encounter: Encounter):
	get_node("/root/Party").save(self.checkpoint)
	bgm_player.play("Stride")
	encounter.disable_player_control()
	encounter.disable_limits()
	for hero in get_tree().get_nodes_in_group("party"):
		match hero.name:
			"Healer", "Swordsman", "Archer":
				hero.visible = false
			"Witch":
				hero.get_node("Life").current = 1
				hero.get_node("Event").move_during(-1, 0.1)
	encounter.camera_in()
	yield(encounter.get_node("Tween"), "tween_completed")
	for hero in get_tree().get_nodes_in_group("party"):
		hero.visible = true
		match hero.name:
			"Healer", "Swordsman", "Archer":
				hero.change_speed(32)
				hero.get_node("Event").move_during(1, 3)
	yield(get_tree().create_timer(3.0), "timeout")
	encounter.enable_player_control([0, 1])
	encounter.enable_limits()
