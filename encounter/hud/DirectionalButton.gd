extends Button

export (String, "move_left", "move_right") var action = "move_left"


# Called when the node enters the scene tree for the first time.
func _ready():
	if not (OS.get_name() in ["Android", "iOS"]):
		queue_free()
	pass

func _process(_delta):
	if self.pressed:
		Input.action_press(self.action)
	else:
		Input.action_release(self.action)
