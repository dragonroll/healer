extends Node

export var shield_scn: PackedScene

signal successfully_cast

func apply_on_character(healer: Character, character: Character):
	var sprite = healer.get_node("Sprite")
	if sprite.can_cast():
		sprite.cast()
		#warning-ignore:return_value_discarded
		sprite.connect("spell_cast", self, "_on_spell_cast", \
									 [healer, weakref(character)], CONNECT_ONESHOT)
		#warning-ignore:return_value_discarded
		healer.connect("damage_taken", self, "_on_damage_taken", [healer], \
									 CONNECT_ONESHOT)

func _on_spell_cast(healer: Character, character_ref: WeakRef):
	if character_ref.get_ref() != null:
		var chara = character_ref.get_ref()
		if chara.has_node("Shield"):
			chara.remove_child(chara.get_node("Shield"))
		var shield = self.shield_scn.instance()
		shield.position.y = -16
		chara.add_child(shield)
		emit_signal("successfully_cast")
	healer.disconnect("damage_taken", self, "_on_damage_taken")

func _on_damage_taken(healer: Character):
	print("cast failed")
	healer.get_node("Sprite").disconnect("spell_cast", self, \
																			 "_on_spell_cast")
