extends TextureButton

class_name Skill

export var healer_path: NodePath
export var cooldown_duration := 1.0
export var free_target := false

func apply_on_character(character: Character):
	if has_node(self.healer_path) and $Cooldown.value <= 0:
		var healer = get_node(self.healer_path)
		for child in get_children():
			if child.has_method("apply_on_character"):
				child.apply_on_character(healer, character)

func cooldown():
	$Cooldown.go(self.cooldown_duration)

func _on_hover():
	if not self.disabled:
		$HoverSFX.play()
