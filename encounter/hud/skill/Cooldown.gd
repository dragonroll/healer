extends ProgressBar

onready var duration = 1.0

func go(the_duration: float):
	self.value = 100
	self.duration = the_duration

func _physics_process(delta):
	self.value -= 100.0 / self.duration * delta

