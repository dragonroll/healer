extends Node

export var power: int = 3

signal successfully_cast

func apply_on_character(healer: Character, character: Character):
	var sprite = healer.get_node("Sprite")
	if sprite.can_cast():
		sprite.cast()
		#warning-ignore:return_value_discarded
		sprite.connect("spell_cast", self, "_on_spell_cast", \
									 [healer, weakref(character)], CONNECT_ONESHOT)
		#warning-ignore:return_value_discarded
		healer.connect("damage_taken", self, "_on_damage_taken", [healer], \
									 CONNECT_ONESHOT)

func _on_spell_cast(healer: Character, character_ref: WeakRef):
	if character_ref.get_ref() != null:
		(character_ref.get_ref() as Character).heal_life(self.power)
		emit_signal("successfully_cast")
	healer.disconnect("damage_taken", self, "_on_damage_taken")

func _on_damage_taken(healer: Character):
	print("cast failed")
	healer.get_node("Sprite").disconnect("spell_cast", self, \
																			 "_on_spell_cast")
