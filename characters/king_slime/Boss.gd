extends Timer


enum {
	LEFT, RIGHT, BEAM, HEAL
}

onready var action = LEFT

func _ready():
	randomize()
	choose()

func choose():
	match self.action:
		LEFT, RIGHT:
			self.action = 2 + randi() % 2
		BEAM, HEAL:
			self.action = randi() % 2
	self.start()

func _process_character(character: Character, _delta):
#	get_node("../Label").text = var2str(self.action) + "/" + var2str(character.dir)
	var sprite = character.get_node("Sprite")
	match self.action:
		LEFT:
			character.dir = -1
		RIGHT:
			character.dir = 1
		BEAM:
			if sprite.can_cast():
				sprite.cast_beam()
		HEAL:
			if sprite.can_cast():
				sprite.cast_heal()

func _on_beam_cast():
	var beam := get_node("../Beam") as Area2D
	for body in beam.get_overlapping_bodies():
		if body.is_in_group("party"):
			body.take_damage(1)
	choose()

func _on_heal_cast():
	get_node("../Life").current += 4
	choose()
