extends Sprite

onready var dying := false

signal death_concluded
signal hit

func _process_character(character: Character, _delta):
	var current_animation: String = $AnimationPlayer.current_animation
	if current_animation == "idle" or current_animation == "walking":
		character.stun = false
		if character.dir == 1:
			character.scale.x = -1
			$AnimationPlayer.play("walking")
		elif character.dir == -1:
			character.scale.x = 1
			$AnimationPlayer.play("walking")
		else:
			$AnimationPlayer.play("idle")
	elif current_animation == "stagger":
		character.stun = true

func emit_hit():
	emit_signal("hit")

func stagger():
	if not self.dying:
		$AnimationPlayer.play("stagger")
		$AnimationPlayer.queue("idle")

func _on_attack_started():
	if $AnimationPlayer.current_animation != "stagger":
		$AnimationPlayer.play("attacking")
		$AnimationPlayer.queue("idle")

func die():
	if not self.dying:
		self.dying = true
		$AnimationPlayer.clear_queue()
		$AnimationPlayer.play("stagger")

func _on_animation_finished(anim_name):
	if anim_name == "stagger" and self.dying:
		emit_signal("death_concluded")
