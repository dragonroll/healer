extends Area2D

enum {
	SEEKING,
	PURSUING,
	PREPARING,
	ATTACKING
}

export(String, "party", "caster", "monster") var target_group = "monster"
export var damage := 1
export var push := 128.0

onready var state = SEEKING
onready var target: WeakRef = null

signal attack_readied
signal attack_started
signal attack_ended

func _process(_delta):
	var states = [ "seeking", "pursuing", "preparing", "attacking" ]
	$Label.text = states[self.state]

func _process_character(character: Character, _delta):
	process_range(character)
	if character.stun:
		self.state = SEEKING
		return
	match self.state:
		SEEKING:
			character.dir = 0
			var closest = find_closest_target(character)
			if closest != null:
				self.target = weakref(closest)
				self.state = PURSUING
		PURSUING:
			if self.target.get_ref() == null:
				self.state = SEEKING
			else:
				if has_target_within_range():
					character.dir = 0
					if $RecoverTimer.is_stopped():
						self.state = PREPARING
						emit_signal("attack_readied")
						#warning-ignore:return_value_discarded
						$PreparationTimer.connect("timeout", self, \
																			"_on_preparation_ended", \
																			[character], CONNECT_ONESHOT)
						$PreparationTimer.start()
				else:
					var target = self.target.get_ref()
					var diff = target.position.x - character.position.x
					character.dir = int(sign(diff))
		_:
			character.dir = 0

func _on_damage_taken():
	self.state = SEEKING
	$PreparationTimer.stop()
	if $PreparationTimer.is_connected("timeout", self, "_on_preparation_ended"):
		$PreparationTimer.disconnect("timeout", self, "_on_preparation_ended")
	if $RecoverTimer.is_connected("timeout", self, "_on_attack_ended"):
		$RecoverTimer.disconnect("timeout", self, "_on_attack_ended")

func _on_preparation_ended(character):
	if self.state == PREPARING:
		self.state = ATTACKING
		emit_signal("attack_started")
		var pulse = sign($Shape.position.x) * self.push
		character.push(pulse)
		#warning-ignore:return_value_discarded
		$RecoverTimer.connect("timeout", self, "_on_attack_ended", [], \
													CONNECT_ONESHOT)
		$RecoverTimer.start()

func _on_attack_ended():
	if self.state == ATTACKING:
		emit_signal("attack_ended")
		self.state = SEEKING

func process_range(character: Character):
	if character.dir != 0:
		$Shape.position.x = sign(character.dir) * abs($Shape.position.x)

func cause_damage():
	for body in get_overlapping_bodies():
		if body is Character and body != self:
			var character := body as Character
			if character.is_in_group(self.target_group):
				character.take_damage(self.damage)

func has_target_within_range():
	for body in get_overlapping_bodies():
		if body is Character and body != self:
			var character := body as Character
			if character.is_in_group(self.target_group):
				return true
	return false

func find_closest_target(character):
	var mindist := INF
	var closest = null
	for node in get_tree().get_nodes_in_group(self.target_group):
		if node is Character:
			var enemy := node as Character
			var dist = abs(enemy.position.x - character.position.x)
			if dist < mindist:
				mindist = dist
				closest = enemy
	return closest
