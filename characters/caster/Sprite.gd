extends Sprite

onready var dying := false

signal spell_cast
signal death_concluded

func _process_character(character: Character, _delta):
	var current_animation: String = $AnimationPlayer.current_animation
	if current_animation == "idle" or current_animation == "walking":
		character.stun = false
		if character.dir == 1:
			self.flip_h = false
			$AnimationPlayer.play("walking")
		elif character.dir == -1:
			self.flip_h = true
			$AnimationPlayer.play("walking")
		else:
			$AnimationPlayer.play("idle")
	elif current_animation in ["stagger", "casting"]:
		character.stun = true

func idle():
	$AnimationPlayer.play("idle")

func stagger():
	if not self.dying:
		$AnimationPlayer.play("stagger")
		$AnimationPlayer.queue("idle")

func cast():
	if $AnimationPlayer.current_animation != "stagger":
		$AnimationPlayer.play("casting")
#		$CastSFX.play()

func can_cast():
	var current_animation = $AnimationPlayer.current_animation
	return current_animation in ["idle", "walking"]

func _on_animation_finished(anim_name):
	if anim_name == "casting":
		emit_signal("spell_cast")
		$AnimationPlayer.queue("idle")
	elif anim_name == "stagger" and dying:
		emit_signal("death_concluded")

func die():
	if not self.dying:
		$AnimationPlayer.clear_queue()
		$AnimationPlayer.play("stagger")
		self.dying = true
