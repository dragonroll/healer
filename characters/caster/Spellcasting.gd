extends Node

export var spell: PackedScene

onready var casting := false

func _process_character(character: Character, _delta):
	$TacticalMap.process_map()
	process_movement(character)
	process_casting(character)

func process_movement(character: Character):
	if not self.casting:
		$TacticalMap.plan_movement_for(character)

func process_casting(character: Character):
	var sprite = character.get_node("Sprite")
	if not self.casting and character.dir == 0 and sprite.can_cast() \
		 and $TacticalMap.has_threats():
		self.casting = true
		sprite.cast()
		#warning-ignore:return_value_discarded
		sprite.connect("spell_cast", self, "_on_spell_cast",
									 [character], CONNECT_ONESHOT)
		character.connect("damage_taken", self, "_on_damage_taken", \
									 [character], CONNECT_ONESHOT)
	elif self.casting and not $TacticalMap.has_threats():
		self.casting = false
		character.disconnect("damage_taken", self, "_on_damage_taken")
		sprite.disconnect("spell_cast", self, "_on_spell_cast")
		sprite.idle()

func _on_spell_cast(character: Character):
	var target = $TacticalMap.plan_strike_for(character)
	if target != null:
		self.casting = false
		var blast = self.spell.instance()
		blast.position = target
		character.spawn(blast)
	character.disconnect("damage_taken", self, "_on_damage_taken")

func _on_damage_taken(character: Character):
	self.casting = false
	character.get_node("Sprite").disconnect("spell_cast", self, \
																					"_on_spell_cast")
