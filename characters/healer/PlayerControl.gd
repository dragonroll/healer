extends Node

onready var enabled := true

func _process_character(character: Character, _delta):
	if self.enabled:
		if Input.is_action_pressed("move_left"):
			character.dir = -1
		elif Input.is_action_pressed("move_right"):
			character.dir = 1
		else:
			character.dir = 0
