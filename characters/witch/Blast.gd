extends Area2D

export (String, "party", "monster") var target_group = "monster"
export var power: int = 1

func boom():
	for body in get_overlapping_bodies():
		if body.is_in_group(self.target_group):
			body.take_damage(self.power)

func die(_x):
	queue_free()
