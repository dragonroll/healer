extends Sprite

export var double_flip := false

func _ready():
	pass

func _process_character(character: Character, _delta):
	var current_animation: String = $AnimationPlayer.current_animation
	if current_animation == "idle" or current_animation == "walking":
		if character.dir == 1:
			self.flip_h = self.double_flip
			$AnimationPlayer.play("walking")
		elif character.dir == -1:
			self.flip_h = not self.double_flip
			$AnimationPlayer.play("walking")
		else:
			$AnimationPlayer.play("idle")

func get_animation() -> AnimationPlayer:
	return $AnimationPlayer as AnimationPlayer
