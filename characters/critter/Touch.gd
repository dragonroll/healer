extends Area2D


export var damage: int = 1
export var push: float = 128.0

func _process_character(character: Character, _delta):
	character.dir = 0
	if $Recover.is_stopped():
		var closest = find_closest_target(character)
		if closest != null:
			var diff: float = closest.position.x - character.position.x
			character.dir = int(sign(diff))
		for body in get_overlapping_bodies():
			if body is Character and body.is_in_group("party"):
				var enemy := body as Character
				enemy.take_damage(self.damage)
				var diff: float = enemy.position.x - character.position.x
				enemy.push(self.push * sign(diff))
				$Recover.start()

func find_closest_target(character: Character):
	var mindist := INF
	var closest = null
	for node in get_tree().get_nodes_in_group("party"):
		if node is Character:
			var enemy := node as Character
			var dist = abs(enemy.position.x - character.position.x)
			if dist < mindist:
				mindist = dist
				closest = enemy
	return closest

