extends Sprite

onready var dying := false

signal shot
signal death_concluded

func _process_character(character: Character, _delta):
	if character.dir == 1:
		self.scale.x = 1
	elif character.dir == -1:
		self.scale.x = -1
	var current_animation: String = $AnimationPlayer.current_animation
	if current_animation in ["idle", "walking"]:
		character.stun = false
		if character.dir != 0:
			$AnimationPlayer.play("walking")
		else:
			$AnimationPlayer.play("idle")
	elif current_animation in ["stagger", "shooting"]:
		character.stun = true

func emit_shot():
	emit_signal("shot")

func idle():
	$AnimationPlayer.play("idle")

func stagger():
	if not self.dying:
		$AnimationPlayer.play("stagger")
		$AnimationPlayer.queue("idle")

func shoot():
	if $AnimationPlayer.current_animation != "stagger":
		$AnimationPlayer.play("shooting")
		$AnimationPlayer.queue("idle")

func can_shoot():
	var current_animation = $AnimationPlayer.current_animation
	return current_animation in ["idle", "walking"]

func _on_animation_finished(anim_name):
	if anim_name == "stagger" and self.dying:
		emit_signal("death_concluded")

func die():
	if not self.dying:
		$AnimationPlayer.clear_queue()
		$AnimationPlayer.play("stagger")
		self.dying = true
