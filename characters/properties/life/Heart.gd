tool
extends TextureRect

class_name Heart

export var filled_tex: Texture
export var empty_tex: Texture
export var filled := true setget set_filled

func _ready():
	self.filled = filled

func set_filled(value: bool):
	filled = value
	if value:
		self.texture = self.filled_tex
	else:
		self.texture = self.empty_tex
