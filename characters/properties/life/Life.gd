tool
extends HBoxContainer

export var heart_scn: PackedScene
export var maximum: int = 3

onready var current: int = self.maximum setget set_current
onready var filled: int = 0
onready var dead := false

signal died

func _ready():
	for _i in range(self.maximum):
		add_child(self.heart_scn.instance())

func set_current(value: int):
	current = max(0, min(self.maximum, value) as int) as int

func is_full() -> bool:
	return self.current == self.maximum and self.filled == self.maximum

func increase_maximum(amount: int):
	self.maximum += amount
	for _i in range(amount):
		add_child(self.heart_scn.instance())

func _update_hearts():
	if self.filled < self.current:
		self.filled += 1
	elif self.filled > self.current:
		self.filled -= 1

func _process_character(_character, _delta):
	if not Engine.editor_hint:
		if not self.dead and self.current == 0 and self.filled == 0:
			self.dead = true
			emit_signal("died")

func _process(_delta):
	if not Engine.editor_hint:
		for i in range(get_child_count()):
			var child = get_child(i)
			if child is Heart:
				var heart := child as Heart
				heart.filled = (i <= self.filled)
