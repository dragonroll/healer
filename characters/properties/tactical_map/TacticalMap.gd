extends Node

const THREAT_WEIGHT = 5
const FILTER = [.1, .2, .5, .9, 1, .9, .5, .2, .1]

export (String, "monster", "party") var target_group = "monster"
export var steps: int = 12

onready var map = []
onready var width = 320 #FIXME Oops
onready var gap = width / self.steps

func _ready():
	self.map.resize(self.steps)

func process_map():
	for i in range(self.steps):
		self.map[i] = 0
	for enemy in get_tree().get_nodes_in_group(self.target_group):
		var x: float = enemy.position.x
		var i := int(x / gap)
		self.map[i] += THREAT_WEIGHT
	var filtered = []
	filtered.resize(self.steps)
	for i in range(self.steps):
		filtered[i] = 0
	for i in range(self.steps):
		for j in range(FILTER.size()):
			var k: int = j - int(FILTER.size() / 2.0)
			if i + k >= 0 and i + k < self.steps:
				filtered[i + k] += self.map[i] * FILTER[j]
	self.map = filtered

func has_threats() -> bool:
	return get_tree().get_nodes_in_group(self.target_group).size() > 0

func plan_movement_for(character: Character):
	var dir := 0
	var k := int(character.position.x / gap)
	var lowest = self.map[k]
	for d in range(-1,2):
		var i = k + d
		if i > 0 and i < self.steps - 1:
			if self.map[i] < lowest:
				dir = d
				lowest = self.map[i]
	if dir == 0:
		var target = plan_strike_for(character)
		if target != null:
			var diff = sign(target.x - character.position.x)
			if self.map[k + diff] <= self.map[k]:
				dir = diff
	character.dir = dir

func plan_strike_for(character: Character):
	if has_threats():
		var greatest = -1
		var k = 0
		for i in range(self.steps):
			var value = self.map[i]
			if value > greatest:
				greatest = value
				k = i
		var x = (k + 0.5) * gap
		var y = character.position.y
		return Vector2(x, y)
	return null
