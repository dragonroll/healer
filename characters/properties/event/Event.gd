extends Timer

onready var active := false
onready var move_dir := 0

func move_during(dir: int, duration: float):
	self.move_dir = dir
	self.wait_time = duration
	self.start()
	yield(self, "timeout")
	self.move_dir = 0

func _process_character(character: Character, _delta):
	if not is_stopped():
		character.dir = self.move_dir
