extends Node2D

export var duration := 5.0
export var improved_ammo: PackedScene
export var improved_blast: PackedScene

func _ready():
	$AnimatedSprite.play()
	
	var hero = get_parent()
	if hero.has_node("Melee"):
		hero.get_node("Melee").damage = 3
	var old_ammo = null
	
	if hero.has_node("Ranged"):
		var ranged = hero.get_node("Ranged")
		old_ammo = ranged.ammo
		ranged.ammo = self.improved_ammo
	
	var old_blast = null
	if hero.has_node("Spellcasting"):
		var spellcasting = hero.get_node("Spellcasting")
		old_blast = spellcasting.spell
		spellcasting.spell = self.improved_blast
	
	if hero.name == "Healer":
		hero.move_speed *= 2
	
	$Timer.wait_time = self.duration
	$Timer.start()
	yield($Timer, "timeout")
	
	if hero.has_node("Melee"):
		hero.get_node("Melee").damage = 1
	
	if old_ammo != null:
		var ranged = hero.get_node("Ranged")
		ranged.ammo = old_ammo
	
	if old_blast != null:
		var spellcasting = hero.get_node("Spellcasting")
		spellcasting.spell = old_blast
	
	if hero.name == "Healer":
		hero.move_speed /= 2
	
	queue_free()
