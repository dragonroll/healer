extends AnimatedSprite

export var duration := 5.0

# Called when the node enters the scene tree for the first time.
func _ready():
	self.play()
	$Timer.wait_time = self.duration
	$Timer.start()
	yield($Timer, "timeout")
	queue_free()

