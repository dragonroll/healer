extends KinematicBody2D

class_name Character

const ACCELERATION = 256
const DAMP_FACTOR = 0.9
const PUSH_REDUCTION = 0.8
const REPULSION_STRENGTH = 2000

export var move_speed := 100.0

signal damage_taken
signal spawn_requested(node, pos)

onready var speed := 0.0
onready var repulsion := 0.0
onready var total_push := 0.0
onready var dir := 0
onready var stun := false

onready var original_speed := move_speed

func _ready():
	$Cursor.play()

func change_speed(value: float):
	self.move_speed = value

func restore_speed():
	self.move_speed = self.original_speed

func _physics_process(delta: float):
	process_properties(delta)
	process_repulsion(delta)
	process_movement(delta)

func process_properties(delta: float):
	for child in get_children():
		if child.has_method("_process_character"):
			child._process_character(self, delta)

func process_repulsion(delta: float):
	self.repulsion = 0.0
	if self.dir == 0:
		for body in $RepulsionArea.get_overlapping_bodies():
			if repulses(body):
				var diff: float = self.position.x - body.position.x
				var intensity = sign(diff)
				if abs(diff) > 1:
					intensity /=  diff * diff
				self.repulsion += intensity * delta * REPULSION_STRENGTH

func repulses(body) -> bool:
	return body.get_script() == self.get_script() and body != self \
		 and body.dir == 0

func push(value: float):
	self.total_push += value

func take_damage(amount: int):
	if not has_node("Shield"):
		$Life.current -= max(0, amount)
		emit_signal("damage_taken")

func heal_life(amount: int):
	$Life.current += max(0, amount)

func process_movement(delta: float):
	self.speed += ACCELERATION * delta
	self.speed = min(self.speed, self.move_speed)
	var movement := self.repulsion + self.total_push
	if not self.stun:
		movement += dir * self.speed
	#warning-ignore:return_value_discarded
	move_and_collide(Vector2(movement, 0) * delta)
	if dir == 0:
		self.speed *= DAMP_FACTOR
	self.total_push *= PUSH_REDUCTION

func focus():
	$Cursor.visible = true

func unfocus():
	$Cursor.visible = false

func spawn(node: Node):
	emit_signal("spawn_requested", node)
