extends Area2D

export var damage: int = 1
export (String, "party", "monster") var target_group = "party"
export var speed := 128.0

var dir: int = 1
var arc := 1.0

onready var t := 0.0
onready var base_y = self.position.y

func _ready():
	$Sprite.scale.x = dir

func _physics_process(delta):
	for body in get_overlapping_bodies():
		if body.is_in_group(self.target_group):
			var chara := body as Character
			chara.take_damage(self.damage)
			self.queue_free()
			return
	self.position.x += dir * speed * delta
	self.t += delta
	self.position.y = self.base_y + t*t * 32 - t * 32
	if self.position.y > self.base_y + 16:
		queue_free()
