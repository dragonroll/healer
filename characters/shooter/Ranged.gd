extends Position2D

export var ammo: PackedScene

onready var shooting:= false
onready var offset := abs(self.position.x)

func _process_character(character: Character, _delta):
	$TacticalMap.process_map()
	process_movement(character)
	process_shooting(character)

func face(dir):
	match dir:
		-1: self.position.x = -self.offset
		1: self.position.x = self.offset

func process_movement(character: Character):
	if not self.shooting:
		$TacticalMap.plan_movement_for(character)
		face(character.dir)
	else:
		var target = $TacticalMap.plan_strike_for(character)
		if target != null:
			var dir = sign(target.x - character.position.x)
			face(dir)
			character.dir = dir

func process_shooting(character: Character):
	var sprite = character.get_node("Sprite")
	if not self.shooting and character.dir == 0 \
		 and sprite.can_shoot() and $TacticalMap.has_threats() \
		 and $Timer.is_stopped():
		self.shooting = true
		sprite.shoot()
		#warning-ignore:return_value_discarded
		sprite.connect("shot", self, "_on_shot",
									 [character], CONNECT_ONESHOT)
		character.connect("damage_taken", self, "_on_damage_taken", \
									 [character], CONNECT_ONESHOT)
	elif self.shooting:
		if not $TacticalMap.has_threats():
			self.shooting = false
			character.disconnect("damage_taken", self, "_on_damage_taken")
			sprite.disconnect("shot", self, "_on_shot")
			sprite.idle()
		else:
			var target = $TacticalMap.plan_strike_for(character)
			var dir = sign(target.x - character.position.x)
			character.dir = dir

func _on_shot(character: Character):
	var target = $TacticalMap.plan_strike_for(character)
	if target != null:
		self.shooting = false
		var bullet = self.ammo.instance()
		var diff = target.x - character.position.x
		var dir = sign(diff)
		bullet.position = character.position + self.position
		bullet.dir = dir
		bullet.arc = abs(diff) / 160
		character.spawn(bullet)
		$Timer.start()
	character.disconnect("damage_taken", self, "_on_damage_taken")

func _on_damage_taken(character: Character):
	self.shooting = false
	character.get_node("Sprite").disconnect("shot", self, \
																					"_on_shot")
